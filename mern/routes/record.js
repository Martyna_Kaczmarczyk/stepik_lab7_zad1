const express = require("express");
const recordRoutes = express.Router();
const dbo = require("../db/conn");
const ObjectId = require("mongodb").ObjectId;




recordRoutes.route("/products").get(async function(req, res) {
        let db_connect = await dbo.getDb("products");
        const {filterByName, sortBy, sortOrder} = req.query

        const filterQuery = {}
        if (filterByName) {
        filterQuery.name = filterByName }
        
        const sortQuery = {} 
        if (sortQuery) {
        sortQuery[sortBy] = sortOrder}
    await db_connect.collection("products").find(filterQuery).sort(sortQuery).toArray(function(err, result) {


        if (err) throw err;
        res.json(result);
    });
});

recordRoutes.route("/products").post(async function(req, res) {
    let db_connect = await dbo.getDb("products");
    let myobj = {
        name: req.body.name,
        price: req.body.price,
        description: req.body.description,
        quantity: req.body.quantity,
        unit_of_measure: req.body.unit_of_measure
    };

   await db_connect.collection("products").find().toArray(async function(err, result) {
        if (err) throw err;
     
        const check = result.some(val => val.name === myobj.name)
        if (!(check)) {
            await db_connect.collection("products").insertOne(myobj, function(err, r) {
                if (err) throw err;
                res.json(r);
            });
         }
        
    });

});


recordRoutes.route("/products/:id").put(async function(req, response){
    let db_connect = await dbo.getDb("products");
    let myquery = {_id: ObjectId(req.params.id)};
    let newValues = {
        $set: {
            name: req.body.name,
            price: req.body.price,
            description:  req.body.description,
            quantity: req.body.quantity,
            unit_of_measure: req.body.unit_of_measure
        },
    };
    await db_connect.collection("products").updateOne(myquery, newValues, function(err, res){
        if (err) throw err;
        console.log("1 document updated successfully");
        response.json(res);
    });
});

recordRoutes.route("/products/:id").delete(async function(req, response){
    let db_connect = await dbo.getDb("products");
    let myquery = {_id: ObjectId(req.params.id)};


    await db_connect.collection("products").deleteOne(myquery, async function(err, res){
        if (err) throw err;
        console.log("1 document deleted successfully");
        response.json(res);
    });

    });
    
recordRoutes.route("/report").get(async function(req, res) {
    let db_connect = await dbo.getDb("products");
    await db_connect.collection("products").aggregate([{
        $project: {
            _id: 0,
            name: 1,
            quantity: 1,
            totalValue: {$sum: {$multiply: ["$quantity", "$price"]}}
        }
    }]).toArray(function(err, result) {
            if (err) throw err;
            const report = result.reduce((acc, val) => {
                const el = {
                    product: val.name,
                    totalQuantity: val.quantity,
                    totalValue: val.totalValue,
                };
                acc.push(el)
                return acc
            }, [])
            res.json(report);
        });
    });


module.exports = recordRoutes;