const {MongoClient} = require("mongodb");
const Db = process.env.MONGO_URI;
console.log(Db)
const client = new MongoClient(Db, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
});

var _db;



module.exports = {
    connectToServer: function(callback) {
        client.connect(function(err, db) {
            if (db){
                _db = db.db("test_example");
                console.log("Successfully connected to MongoDB");
                del();
                createCollection();
                insertDocuments();
                
 
            }
            return callback(err);
        });
    },
    getDb: function() {
        return _db;
    },
};

function createCollection() {
    _db.createCollection("products", function (err, res) {
        if (err) throw err;
        console.log("Collection 'products' created!");
    });
}
function del() {
    try {
        _db.collection("products").deleteMany(function (err, res) {
            if (err) throw err;
            console.log("Documents deleted");
        });
    } catch {
        console.log("Nothing to delete")
    }

}
function insertDocuments() {

    const sample = [
        {
            name: "strawberry",
            price: 10.00,
            description: "Sweet, healthy, fresh",
            quantity: 30,
            unit_of_measure: "kg",
        },
        {
            name: "tree",
            price: 20.00,
            description: "Green, coniferous, fresh",
            quantity: 100,
            unit_of_measure: "-",
        },
        {
            name: "car",
            price: 1000.00,
            description: "Second-hand, Skoda, 2nd-class",
            quantity: 5,
            unit_of_measure: "-",
        },
        {
            name: "sweets",
            price: 10.00,
            description: "Jelly candies, śmiej żelki",
            quantity: 200,
            unit_of_measure: "packs",
        },
    ];

    _db.collection("products").insertMany(sample, function (err, res) {
        if (err) throw err;
        console.log("Documents inserted:", res.insertedCount);
    });
}